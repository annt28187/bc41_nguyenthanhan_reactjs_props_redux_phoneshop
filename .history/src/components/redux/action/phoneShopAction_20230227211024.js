import {
  ADD_TO_CART,
  DELETE_TO_CART,
  CHANGE_QUANTITY,
  SHOW_DETAIL,
} from '../constant/phoneShopConstant';

export const addToCartAction = (phone) => ({
  type: ADD_TO_CART,
  payload: phone,
});

export const deleteToCartAction = (idPhone) => ({
  type: DELETE_TO_CART,
  payload: idPhone,
});

export const changeQuantityAction = (idPhone, luaChon) => ({
  type: CHANGE_QUANTITY,
  payload: {
    idPhone: idPhone,
    luaChon: luaChon,
  },
});

export const showDetailAction = (phone) => ({
  type: SHOW_DETAIL,
  payload: phone,
});
