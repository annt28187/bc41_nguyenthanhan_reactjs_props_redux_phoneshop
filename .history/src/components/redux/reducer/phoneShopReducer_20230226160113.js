import { dataPhone } from '../../data_phone';

let initialValue = {
  dataPhone: dataPhone,
  detail: dataPhone[0],
  cart: [],
};

export const phoneShopReducer = (state = initialValue, action) => {
  switch (action.type) {
    default:
      return state;
  }
};
