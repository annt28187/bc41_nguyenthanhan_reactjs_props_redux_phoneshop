import { dataPhone } from '../../data_phone';
import {} from '../constant/phoneShopConstant';

let initialValue = {
  dataPhone: dataPhone,
  detail: dataPhone[0],
  cart: [],
};

export const phoneShopReducer = (state = initialValue, action) => {
  switch (action.type) {
    default:
      return state;
  }
};
