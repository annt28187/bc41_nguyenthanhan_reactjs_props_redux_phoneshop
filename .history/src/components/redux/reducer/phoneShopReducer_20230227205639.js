import { dataPhone } from '../../data_phone';
import {
  ADD_TO_CART,
  DELETE_TO_CART,
  CHANGE_QUANTITY,
  SHOW_DETAIL,
} from '../constant/phoneShopConstant';

let initialValue = {
  dataPhone: dataPhone,
  detail: dataPhone[0],
  cart: [],
};

export const phoneShopReducer = (state = initialValue, action) => {
  switch (action.type) {
    default:
      return state;
  }
};
