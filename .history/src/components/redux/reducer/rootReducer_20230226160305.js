import { combineReducers } from 'redux';
import { phoneShopReducer } from './phoneShopReducer';

export const rootReducer = combineReducers({
  phoneShopReducer: phoneShopReducer,
});
