import { dataPhone } from '../../data_phone';
import {
  ADD_TO_CART,
  DELETE_TO_CART,
  CHANGE_QUANTITY,
  SHOW_DETAIL,
} from '../constant/phoneShopConstant';

let initialValue = {
  dataPhone: dataPhone,
  detail: dataPhone[0],
  cart: [],
};

export const phoneShopReducer = (state = initialValue, action) => {
  switch (action.type) {
    case ADD_TO_CART: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.maSP === action.payload.maSP;
      });
      if (index === -1) {
        let cartItem = { ...action.payload, soLuong: 1 };
        cloneCart.push(cartItem);
      } else {
        cloneCart[index].soLuong++;
      }
      return { ...state, cart: cloneCart };
    }
    case DELETE_TO_CART: {
      let newCart = this.state.cart.filter((phone) => {
        return phone.maSP !== action.payload;
      });
      return { ...state, cart: newCart };
    }
    default:
      return state;
  }
};
