import React, { Component } from 'react';
import { connect } from 'react-redux';

class DetailPhone extends Component {
  render() {
    let { tenSP, manHinh, heDieuHanh, cameraTruoc, cameraSau, ram, rom, giaBan, hinhAnh } =
      this.props.detail;
    return (
      <div>
        <div className="row mt-5 p-5 text-left">
          <div className="col-4">
            <h4 className="p-5">{tenSP}</h4>
            <img class="img-fluid" src={hinhAnh} alt={tenSP} />
          </div>
          <div className="col-8">
            <h4 className="p-5">Thông số kỹ thuật</h4>
            <div className="row">
              <div className="col-3">
                <p>Màn hình</p>
                <p>Hệ điều hành</p>
                <p>Camera trước</p>
                <p>Camera sau</p>
                <p>RAM</p>
                <p>ROM</p>
                <p>Giá bán</p>
              </div>
              <div className="col-5">
                <p>{manHinh}</p>
                <p>{heDieuHanh}</p>
                <p>{cameraTruoc}</p>
                <p>{cameraSau}</p>
                <p>{ram}</p>
                <p>{rom}</p>
                <p>{giaBan} VNĐ</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    detail: state.phoneShopReducer.detail,
  };
};

export default connect()(DetailPhone);
