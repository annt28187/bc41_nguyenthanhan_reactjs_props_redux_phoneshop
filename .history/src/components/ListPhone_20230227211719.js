import React, { Component } from 'react';
import { connect } from 'react-redux';
import ItemPhone from './ItemPhone';

class ListPhone extends Component {
  renderListPhone = () => {
    return this.props.list.map((item, index) => {
      return <ItemPhone key={index} phone={item} />;
    });
  };
  render() {
    return <div className="row">{this.renderListPhone()}</div>;
  }
}

let mapStateToProps = (state) => {
  return {
    list: state.phoneShopReducer.listPhone,
  };
};

export default connect(mapStateToProps)(ListPhone);
