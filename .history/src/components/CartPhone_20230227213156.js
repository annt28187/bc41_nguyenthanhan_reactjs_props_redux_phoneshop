import React, { Component } from 'react';
import { connect } from 'react-redux';
import { changeQuantityAction } from './../../.history/src/components/redux/action/phoneShopAction_20230227210918';
import { deleteToCartAction } from './redux/action/phoneShopAction';

class CartPhone extends Component {
  renderTbody = () => {
    return this.props.cart.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.maSP}</td>
          <td>
            <img src={item.hinhAnh} style={{ width: 50 }} alt={item.tenSP} />
          </td>
          <td>{item.tenSP}</td>
          <td>
            <button
              className="btn btn-danger"
              onClick={() => {
                this.props.handleChangeQuantity(item.maSP, -1);
              }}
            >
              -
            </button>
            <strong className="mx-2">{item.soLuong}</strong>
            <button
              className="btn btn-success"
              onClick={() => {
                this.props.handleChangeQuantity(item.maSP, 1);
              }}
            >
              +
            </button>
          </td>
          <td>{item.giaBan}</td>
          <td>{item.giaBan * item.soLuong}</td>
          <td>
            <button
              className="btn btn-warning"
              onClick={() => {
                this.props.handleDeleteToCart(item.maSP);
              }}
            >
              Delete
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <th>Mã sản phẩm</th>
            <th>Hình ảnh</th>
            <th>Tên sản phẩm</th>
            <th>Số lượng</th>
            <th>Đơn giá</th>
            <th>Thành tiền</th>
            <th>Hành động</th>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    cart: state.phoneShopReducer.cart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleChangeQuantity: (idPhone, luaChon) => {
      dispatch(changeQuantityAction(idPhone, luaChon));
    },
    handleDeleteToCart: (phone) => {
      dispatch(deleteToCartAction(phone));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CartPhone);
