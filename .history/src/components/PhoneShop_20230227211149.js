import React, { Component } from 'react';
import CartPhone from './CartPhone';
import DetailPhone from './DetailPhone';
import ListPhone from './ListPhone';

export default class PhoneShop extends Component {
  render() {
    return (
      <div className="container">
        <h2 className="p-4">Phone Shop</h2>
        <CartPhone />
        <ListPhone />
        <DetailPhone />
      </div>
    );
  }
}
