import React, { Component } from 'react';
import { connect } from 'react-redux';

class ItemPhone extends Component {
  render() {
    let { hinhAnh, tenSP, giaBan } = this.props.phone;
    return (
      <div className="col-4 p-4">
        <div className="card border-primary h-100">
          <img className="card-img-top" src={hinhAnh} alt={tenSP} />
          <div className="card-body">
            <h4 className="card-title">{tenSP}</h4>
            <p className="card-text">{giaBan} VNĐ</p>
            <button
              className="btn btn-success mx-2"
              onClick={() => {
                this.props.handleChangeClick(this.props.phone);
              }}
            >
              Xem chi tiết
            </button>
            <button
              className="btn btn-danger"
              onClick={() => {
                this.props.handleAddClick(this.props.phone);
              }}
            >
              Thêm giỏ hàng
            </button>
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {};

export default connect(null, mapDispatchToProps)(ItemPhone);
