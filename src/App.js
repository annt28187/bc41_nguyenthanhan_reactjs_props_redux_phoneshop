import './App.css';
import PhoneShop from './components/PhoneShop';

function App() {
  return (
    <div className="App">
      <PhoneShop />
    </div>
  );
}

export default App;
